#!/usr/bin/python

from __future__ import division

import hashlib
import logging
import operator
import re
import socket
import textile
import tornado.escape
import tornado.locale
import tornado.web
import unicodedata

import backend

class UIModule(tornado.web.UIModule):
	@property
	def accounts(self):
		return self.handler.accounts

	@property
	def advertisements(self):
		return self.handler.advertisements

	@property
	def banners(self):
		return self.handler.banners

	@property
	def memcached(self):
		return self.handler.memcached

	@property
	def releases(self):
		return self.handler.releases

	@property
	def geoip(self):
		return self.handler.geoip

	@property
	def news(self):
		return self.handler.news

	@property
	def planet(self):
		return self.handler.planet

	@property
	def talk(self):
		return self.handler.talk

	@property
	def wishlist(self):
		return self.handler.wishlist


class AdvertisementModule(UIModule):
	def render(self, where):
		assert where in ("download-splash",), where

		ad = self.advertisements.get(where)
		if not ad:
			return ""

		# Mark that advert has been shown.
		ad.update_impressions()

		return self.render_string("modules/ads/%s.html" % where, ad=ad)


class FireinfoDeviceTableModule(UIModule):
	def render(self, devices):
		return self.render_string("fireinfo/modules/table-devices.html",
				devices=devices)


class FireinfoDeviceAndGroupsTableModule(UIModule):
	def render(self, devices):
		_ = self.locale.translate

		groups = {}

		for device in devices:
			if not groups.has_key(device.cls):
				groups[device.cls] = []

			groups[device.cls].append(device)

		# Sort all devices
		for key in groups.keys():
			groups[key].sort()

		# Order the groups by their name
		groups = groups.items()
		groups.sort()

		return self.render_string("fireinfo/modules/table-devices-and-groups.html",
				groups=groups)


class FireinfoGeoTableModule(UIModule):
	def render(self, items):
		countries = []
		other_countries = []
		for code, value in items:
			# Skip the satellite providers in this ranking
			if code in (None, "A1", "A2"):
				continue

			name = self.geoip.get_country_name(code)

			# Don't add countries with a small share on the list
			if value < 0.01:
				other_countries.append(name)
				continue

			country = backend.database.Row({
				"code"  : code,
				"name"  : name,
				"value" : value,
			})
			countries.append(country)

		return self.render_string("fireinfo/modules/table-geo.html",
			countries=countries, other_countries=other_countries)


class LanguageNameModule(UIModule):
	def render(self, language):
		_ = self.locale.translate

		if language == "de":
			return _("German")
		elif language == "en":
			return _("English")
		elif language == "es":
			return _("Spanish")
		elif language == "fr":
			return _("French")
		elif language == "it":
			return _("Italian")
		elif language == "nl":
			return _("Dutch")
		elif language == "pl":
			return _("Polish")
		elif language == "pt":
			return _("Portuguese")
		elif language == "ru":
			return _("Russian")
		elif language == "tr":
			return _("Turkish")

		return language


class MapModule(UIModule):
	def render(self, latitude, longitude):
		return self.render_string("modules/map.html", latitude=latitude, longitude=longitude)


class MenuModule(UIModule):
	def render(self):
		return self.render_string("modules/menu.html")


class MirrorItemModule(UIModule):
	def render(self, item):
		return self.render_string("modules/mirror-item.html", item=item)


class MirrorsTableModule(UIModule):
	def render(self, mirrors, preferred_mirrors=[]):
		return self.render_string("modules/mirrors-table.html",
			mirrors=mirrors, preferred_mirrors=preferred_mirrors)


class NetBootMenuConfigModule(UIModule):
	def render(self, release, arch=None, platform=None):
		return self.render_string("netboot/menu-config.cfg", release=release,
			arch=arch, platform=platform)


class NetBootMenuHeaderModule(UIModule):
	def render(self, title, releases, arch=None, platform=None):
		id = unicodedata.normalize("NFKD", unicode(title)).encode("ascii", "ignore")
		id = re.sub(r"[^\w]+", " ", id)
		id = "-".join(id.lower().strip().split())

		return self.render_string("netboot/menu-header.cfg", id=id,
			title=title, releases=releases, arch=arch, platform=platform)


class NetBootMenuSeparatorModule(UIModule):
	def render(self):
		return self.render_string("netboot/menu-separator.cfg")


class NewsItemModule(UIModule):
	def get_author(self, author):
		# Get name of author
		author = self.accounts.find(author)
		if author:
			return author.name
		else:
			_ = self.locale.translate
			return _("Unknown author")

	def render(self, item, uncut=True, announcement=False, show_heading=True):
		# Get author
		item.author = self.get_author(item.author_id)

		if not uncut and len(item.text) >= 400:
			item.text = item.text[:400] + "..."

		# Render text
		item.text = textile.textile(item.text.decode("utf8"))

		# Find a release if one exists
		release = self.releases.get_by_news_id(item.uuid)

		return self.render_string("modules/news-item.html", item=item, release=release,
			uncut=uncut, announcement=announcement, show_heading=show_heading)


class NewsLineModule(UIModule):
	def render(self, item):
		return self.render_string("modules/news-line.html", item=item)


class NewsTableModule(UIModule):
	def render(self, news):
		return self.render_string("modules/news-table.html", news=news)


class NewsYearNavigationModule(UIModule):
	def render(self, active=None):
		try:
			active = int(active)
		except:
			active = None

		return self.render_string("modules/news-year-nav.html",
			active=active, years=self.news.years)


class PlanetSearchBoxModule(UIModule):
	def render(self, query=None):
		return self.render_string("modules/planet/search-box.html", query=query)


class SidebarItemModule(UIModule):
	def render(self):
		return self.render_string("modules/sidebar-item.html")


class SidebarReleaseModule(UIModule):
	def render(self):
		return self.render_string("modules/sidebar-release.html",
			latest=self.releases.get_latest())


class ReleaseItemModule(UIModule):
	def render(self, release, latest=False):
		arches = ("x86_64", "i586", "arm")

		downloads = []
		for arch in arches:
			files = []

			for file in release.files:
				if not file.arch == arch:
					continue

				files.append(file)

			if files:
				downloads.append((arch, files))

		return self.render_string("modules/release-item.html",
			release=release, latest=latest, downloads=downloads)


class SidebarBannerModule(UIModule):
	def render(self, item=None):
		if not item:
			item = self.banners.get_random()

		return self.render_string("modules/sidebar-banner.html", item=item)


class DownloadButtonModule(UIModule):
	def render(self, release, text="Download now!"):
		best_image = None

		for file in release.files:
			if (release.sname < "ipfire-2.19-core100" or file.arch == "x86_64") \
					and file.type == "iso":
				best_image = file
				break

		# Show nothing when there was no image found.
		if not best_image:
			return ""

		return self.render_string("modules/download-button.html",
			release=release, image=best_image)


class PlanetAuthorBoxModule(UIModule):
	def render(self, author):
		return self.render_string("planet/modules/author-box.html", author=author)


class PlanetEntryModule(UIModule):
	def render(self, entry, show_avatar=True):
		return self.render_string("modules/planet-entry.html",
			entry=entry, show_avatar=show_avatar)


class ProgressBarModule(UIModule):
	def render(self, value, colour=None):
		value *= 100

		return self.render_string("modules/progress-bar.html",
			colour=colour, value=value)


class TalkCallLogModule(UIModule):
	def render(self, account=None, viewer=None):
		if (account is None or not self.current_user == account) \
				and not self.current_user.is_admin():
			raise RuntimeException("Insufficient permissions")

		if viewer is None:
			viewer = self.current_user

		calls = self.talk.get_call_log(account)

		return self.render_string("talk/modules/call-log.html",
			calls=calls, viewer=viewer)


class TalkLinesModule(UIModule):
	def render(self, account=None, show_account=False):
		if (account is None or not self.current_user == account) \
				and not self.current_user.is_admin():
			raise RuntimeException("Insufficient permissions")

		lines = self.talk.get_lines(account)

		return self.render_string("talk/modules/lines.html",
			show_account=show_account, lines=lines)


class TalkOngoingCallsModule(UIModule):
	def render(self, account=None):
		if (account is None or not self.current_user == account) \
				and not self.current_user.is_admin():
			raise RuntimeException("Insufficient permissions")

		calls = self.talk.get_ongoing_calls(account)

		if calls:
			return self.render_string("talk/modules/ongoing-calls.html",
				calls=calls)

		return ""


class TrackerPeerListModule(UIModule):
	def render(self, peers):
		# Guess country code and hostname of the host
		for peer in peers:
			country_code = self.geoip.get_country(peer["ip"])
			if country_code:
				peer["country_code"] = country_code.lower()
			else:
				peer["country_code"] = "unknown"

			try:
				peer["hostname"] = socket.gethostbyaddr(peer["ip"])[0]
			except:
				peer["hostname"] = ""

		return self.render_string("modules/tracker-peerlist.html",
			peers=[backend.database.Row(p) for p in peers])


class WishlistModule(UIModule):
	def render(self, wishes, short=False):
		return self.render_string("wishlist/modules/wishlist.html",
			wishes=wishes, short=short)


class WishModule(UIModule):
	def render(self, wish, short=False):
		progress_bar = "progress-bar-warning"

		if wish.percentage >= 100:
			progress_bar = "progress-bar-success"

		return self.render_string("wishlist/modules/wish.html",
			wish=wish, short=short, progress_bar=progress_bar)


class WishlistItemsModule(UIModule):
	def render(self, wishlist_items):
		return self.render_string("modules/wishlist-items.html",
			wishlist_items=wishlist_items)


class DonationBoxModule(UIModule):
	def render(self, reason_for_transfer=None):
		if reason_for_transfer:
			reason_for_transfer = "IPFire.org - %s" % reason_for_transfer

		return self.render_string("modules/donation-box.html",
			reason_for_transfer=reason_for_transfer)


class DonationButtonModule(UIModule):
	# https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/
	COUNTRIES = (
		"AU",
		"AT",
		"BE",
		"BR",
		"CA",
		"CH",
		"CN",
		"DE",
		"ES",
		"GB",
		"FR",
		"IT",
		"NL",
		"PL",
		"PT",
		"RU",
		"US",
	)

	LOCALES = (
		"da_DK",
		"he_IL",
		"id_ID",
		"ja_JP",
		"no_NO",
		"pt_BR",
		"ru_RU",
		"sv_SE",
		"th_TH",
		"zh_CN",
		"zh_HK",
		"zh_TW",
	)

	def render(self, reason_for_transfer=None, currency="EUR"):
		if not reason_for_transfer:
			reason_for_transfer = "IPFire.org"

		primary = (currency == "EUR")

		return self.render_string("modules/donation-button.html", primary=primary,
			reason_for_transfer=reason_for_transfer, currency=currency, lc=self.lc)

	@property
	def lc(self):
		"""
			Returns the locale of the user
		"""
		try:
			locale, delimiter, encoding = self.locale.code.partition(".")

			# Break for languages in specific countries
			if locale in self.LOCALES:
				return locale

			lang, delimiter, country_code = locale.partition("_")

			if country_code and country_code in self.COUNTRIES:
				return country_code

			lang = lang.upper()
			if lang in self.COUNTRIES:
				return lang
		except:
			pass

		# If anything goes wrong, fall back to GB
		return "GB"


class DonationInputBoxModule(DonationButtonModule):
	def render(self):
		currencies = ("EUR", "USD", "GBP", "CHF", "AUD", "NZD", "CAD")

		return self.render_string("modules/donation-input-box.html",
			currencies=currencies, lc=self.lc)
