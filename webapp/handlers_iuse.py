#!/usr/bin/python

import datetime
import logging
import os
import random
import tornado.web

from handlers_base import *
import backend

class IUseImage(BaseHandler):
	expires = 3600 * 3

	def write_error(self, status_code, **kwargs):
		"""
			Select a random image from the errors directory
			and return the content.
		"""
		self.set_header("Content-Type", "image/png")

		template_path = self.application.settings.get("template_path", "")
		template_path = os.path.join(template_path, "i-use", "errors")

		images = os.listdir(template_path)
		if images:
			image = random.choice(images)
			image = os.path.join(template_path, image)

			imgdata = None
			with open(image, "rb") as f:
				imgdata = f.read()

			self.finish(imgdata)

	def get(self, profile_id, image_id):
		image = None

		when = self.get_argument_date("when", None)

		# Try to get the image from memcache. If we have a cache miss we
		# build a new one.
		mem_id = "iuse-%s-%s-%s" % (profile_id, image_id, self.locale.code)
		if when:
			mem_id += "-%s" % when.isoformat()

		cache = self.get_argument("cache", "true")
		if cache == "true":
			image = self.memcached.get(mem_id)

		if image:
			logging.debug("Got image from cache for profile: %s" % profile_id)
		else:
			logging.info("Rendering new image for profile: %s" % profile_id)

			profile = self.fireinfo.get_profile_with_data(profile_id, when=when)
			if not profile:
				raise tornado.web.HTTPError(404, "Profile '%s' was not found." % profile_id)

			image_cls = self.iuse.get_imagetype(image_id)
			if not image_cls:
				raise tornado.web.HTTPError(404, "Image class is unknown: %s" % image_id)

			# Render the image
			image = image_cls(self.backend, self, profile).to_string()

			# Save the image to the memcache
			self.memcached.set(mem_id, image, self.expires)

		self.set_header("Content-Type", "image/png")

		# Set expiry headers
		self.set_header("Expires",
			datetime.datetime.utcnow() + datetime.timedelta(seconds=self.expires))
		self.set_header("Cache-Control", "public,max-age=%d" % self.expires)

		self.write(image)
