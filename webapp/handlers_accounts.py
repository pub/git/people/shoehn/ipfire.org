#!/usr/bin/python

import logging
import tornado.web

from handlers_base import *

class AccountsAvatarHandler(BaseHandler):
	def get(self, who):
		# Get the desired size of the avatar file
		size = self.get_argument("size", 0)

		try:
			size = int(size)
		except (TypeError, ValueError):
			size = None

		# Cache handle
		cache_handle = "accounts-avatar-%s-%s" % (who, size or 0)
		avatar = self.memcached.get(cache_handle)

		if not avatar:
			logging.debug("Querying for avatar of %s" % who)

			account = self.backend.accounts.get_by_uid(who)
			if not account:
				raise tornado.web.HTTPError(404)

			avatar = account.get_avatar(size)

			# Save the avatar to cache for 6 hours
			if avatar:
				self.memcached.set(cache_handle, avatar, 6 * 3600)

			# Otherwise redirect to gravatar
			else:
				avatar = account.get_gravatar_url(size)

		if avatar.startswith("http://"):
			return self.redirect(avatar)

		self.set_header("Cache-Control", "public,max-age=300")
		self.set_header("Content-Disposition", "inline; filename=\"%s.jpg\"" % who)
		self.set_header("Content-Type", "image/jpeg")

		self.finish(avatar)
