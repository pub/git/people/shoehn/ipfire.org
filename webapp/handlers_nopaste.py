#!/usr/bin/python

import tornado.web

from handlers_base import *

class NopasteCreateHandler(BaseHandler):
	MODES = ("paste", "upload")

	def get(self):
		mode = self.get_argument("mode", "paste")
		if not mode in self.MODES:
			raise tornado.web.HTTPError(400)

		self.render("nopaste/create.html", mode=mode,
			max_size=self._max_size)

	def post(self):
		mode = self.get_argument("mode")
		if not mode in self.MODES:
			raise tornado.web.HTTPError(400)

		if mode == "paste":
			subject = self.get_argument("subject", None)
			content = self.get_argument("content")
			type = "text"

		elif mode == "upload":
			type = "file"

			for f in self.request.files.get("file"):
				subject = f.filename
				content = f.body
				break

		# Check maximum size
		if len(content) > self._max_size:
			raise tornado.web.HTTPError(400,
				"You cannot upload more than %s bytes" % self._max_size)

		expires = self.get_argument("expires", "0")
		try:
			expires = int(expires)
		except (TypeError, ValueError):
			expires = None

		uid = self.backend.nopaste.create(subject, content, type=type,
			expires=expires, account=self.current_user,
			address=self.get_remote_ip())

		if uid:
			return self.redirect("/view/%s" % uid)

		raise tornado.web.HTTPError(500)

	@property
	def _max_size(self):
		# Authenticated users are allowed to upload up to 10MB
		if self.current_user:
			return 25 * (1024 ** 2)

		# The rest is only allowed to upload up to 2MB
		return 2 * (1024 ** 2)


class NopasteRawHandler(BaseHandler):
	def get(self, uid):
		entry = self.backend.nopaste.get(uid)
		if not entry:
			raise tornado.web.HTTPError(404)

		# Set the filename
		self.set_header("Content-Disposition", "inline; filename=\"%s\"" % entry.subject)

		# Set mimetype
		self.set_header("Content-Type", entry.mimetype)

		# Set expiry headers
		expires = entry.time_expires or \
			(datetime.datetime.utcnow() + datetime.timedelta(days=30))

		# For HTTP/1.0
		self.set_header("Expires", expires)

		# For HTTP/1.1
		max_age = expires - datetime.datetime.utcnow()
		self.set_header("Cache-Control", "public,max-age=%d" % max_age.total_seconds())

		# Send content
		content = self.backend.nopaste.get_content(entry.uuid)
		self.finish(content)


class NopasteViewHandler(BaseHandler):
	def get(self, uid):
		entry = self.backend.nopaste.get(uid)
		if not entry:
			raise tornado.web.HTTPError(404)

		# Fetch the content if the output should be displayed
		if entry.mimetype.startswith("text/"):
			content = self.backend.nopaste.get_content(entry.uuid)
		else:
			content = None

		self.render("nopaste/view.html", entry=entry, content=content)
