#!/usr/bin/python

import tornado.web

from handlers_base import *

class TalkIndexHandler(BaseHandler):
	@tornado.web.authenticated
	def get(self):
		call_log = self.talk.get_call_log(self.current_user, limit=6)
		favourite_contacts = self.talk.get_favourite_contacts(self.current_user)

		if self.current_user.is_admin():
			ongoing_calls = self.talk.get_ongoing_calls()
		else:
			ongoing_calls = self.talk.get_ongoing_calls(self.current_user)

		self.render("talk/index.html", call_log=call_log,
			favourite_contacts=favourite_contacts, ongoing_calls=ongoing_calls)


class TalkPhonebookHandler(BaseHandler):
	@tornado.web.authenticated
	def get(self):
		phonebook = self.talk.get_phonebook(self.current_user)

		self.render("talk/phonebook.html", phonebook=phonebook)


class TalkPhonebookAccountHandler(BaseHandler):
	@tornado.web.authenticated
	def get(self, uid):
		account = self.accounts.find(uid)
		if not account:
			raise tornado.web.HTTPError(404, "Account not found: %s" % uid)

		self.render("talk/phonebook-contact.html", account=account)


class TalkDiagnosisHandler(BaseHandler):
	@tornado.web.authenticated
	def get(self):
		return self.render("talk/diagnosis.html")


class TalkConferencesHandler(BaseHandler):
	@tornado.web.authenticated
	def get(self):
		return self.render("talk/conferences.html",
			conferences=self.talk.conferences)


class TalkConferencesInviteHandler(BaseHandler):
	@tornado.web.authenticated
	def get(self, conference_id, invitee_id):
		# Check for the conference room
		conference = self.talk.get_conference(conference_id)
		if not conference:
			raise tornado.web.HTTPError(400)

		# Ccheck for the invitee
		invitee = self.accounts.get_by_sip_id(invitee_id)
		if not invitee:
			raise tornado.web.HTTPError(400)

		# Initiate call
		self.talk.initiate_call(invitee.sip_id, conference_id)

		self.render("talk/conference-invite.html", invitee=invitee,
			conference=conference)


class TalkInitiateCallHandler(BaseHandler):
	@tornado.web.authenticated
	def get(self, called_id, caller=None):
		if not self.current_user.sip_id:
			raise tornado.web.HTTPError(400)

		called = self.accounts.get_by_sip_id(called_id)

		self.talk.initiate_call(self.current_user.sip_id, called_id)

		next = self.get_argument("next", None)
		if next is None:
			next = self.request.headers.get("Referer")

		self.render("talk/initiate-call.html", caller=self.current_user,
			called=called, called_id=called_id, next=next)


class TalkProfileHandler(BaseHandler):
	@tornado.web.authenticated
	def get(self):
		return self.redirect("/phonebook/%s" % self.current_user.uid)
