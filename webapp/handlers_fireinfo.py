#!/usr/bin/python

from __future__ import division

import datetime
import hwdata
import logging
import re
import simplejson
import tornado.web

import backend

from handlers_base import *

class FireinfoBaseHandler(BaseHandler):
	def cut_string(self, s, limit=15):
		if len(s) > limit:
			s = s[:limit] + "..."

		return s

	def render(self, *args, **kwargs):
		kwargs.update({
			"cut_string" : self.cut_string,
		})

		return BaseHandler.render(self, *args, **kwargs)

	@property
	def when(self):
		return self.get_argument_date("when", None)


MIN_PROFILE_VERSION = 0
MAX_PROFILE_VERSION = 0

class Profile(dict):
	def __getattr__(self, key):
		try:
			return self[key]
		except KeyError:
			raise AttributeError, key

	def __setattr__(self, key, val):
		self[key] = val


class FireinfoProfileSendHandler(FireinfoBaseHandler):
	def check_xsrf_cookie(self):
		# This cookie is not required here.
		pass

	def prepare(self):
		# Create an empty profile.
		self.profile = Profile()

	def __check_attributes(self, profile):
		"""
			Check for attributes that must be provided,
		"""
		attributes = (
			"private_id",
			"profile_version",
			"public_id",
		)
		for attr in attributes:
			if not profile.has_key(attr):
				raise tornado.web.HTTPError(400, "Profile lacks '%s' attribute: %s" % (attr, profile))

	def __check_valid_ids(self, profile):
		"""
			Check if IDs contain valid data.
		"""
		for id in ("public_id", "private_id"):
			if re.match(r"^([a-f0-9]{40})$", "%s" % profile[id]) is None:
				raise tornado.web.HTTPError(400, "ID '%s' has wrong format: %s" % (id, profile))

	def __check_equal_ids(self, profile):
		"""
			Check if public_id and private_id are equal.
		"""
		if profile.public_id == profile.private_id:
			raise tornado.web.HTTPError(400, "Public and private IDs are equal: %s" % profile)

	def __check_matching_ids(self, profile):
		"""
			Check if a profile with the given public_id is already in the
			database. If so we need to check if the private_id matches.
		"""
		p = self.profiles.find_one({ "public_id" : profile["public_id"]})
		if not p:
			return

		p = Profile(p)
		if p.private_id != profile.private_id:
			raise tornado.web.HTTPError(400, "Mismatch of private_id: %s" % profile)

	def __check_profile_version(self, profile):
		"""
			Check if this version of the server software does support the
			received profile.
		"""
		version = profile.profile_version

		if version < MIN_PROFILE_VERSION or version > MAX_PROFILE_VERSION:
			raise tornado.web.HTTPError(400,
				"Profile version is not supported: %s" % version)

	def check_profile_blob(self, profile):
		"""
			This method checks if the blob is sane.
		"""
		checks = (
			self.__check_attributes,
			self.__check_valid_ids,
			self.__check_equal_ids,
			self.__check_profile_version,
			# These checks require at least one database query and should be done
			# at last.
			self.__check_matching_ids,
		)

		for check in checks:
			check(profile)

		# If we got here, everything is okay and we can go on...

	def get_profile_blob(self):
		profile = self.get_argument("profile", None)

		# Send "400 bad request" if no profile was provided
		if not profile:
			raise tornado.web.HTTPError(400, "No profile received")

		# Try to decode the profile.
		try:
			return simplejson.loads(profile)
		except simplejson.decoder.JSONDecodeError, e:
			raise tornado.web.HTTPError(400, "Profile could not be decoded: %s" % e)

	# The GET method is only allowed in debugging mode.
	def get(self, public_id):
		if not self.application.settings["debug"]:
			return tornado.web.HTTPError(405)

		return self.post(public_id)

	def post(self, public_id):
		profile_blob = self.get_profile_blob()
		#self.check_profile_blob(profile_blob)

		# Get location
		location = self.get_remote_location()
		if location:
			location = location.country

		# Handle the profile.
		try:
			self.fireinfo.handle_profile(public_id, profile_blob, location=location)

		except backend.fireinfo.ProfileParserError:
			raise tornado.web.HTTPError(400)

		self.finish("Your profile was successfully saved to the database.")


class FireinfoIndexHandler(FireinfoBaseHandler):
	def _profile_not_found(self, profile_id):
		self.set_status(404)
		self.render("fireinfo/profile-notfound.html", profile_id=profile_id)

	def get(self):
		self.render("fireinfo/index.html")

	def post(self):
		profile_id = self.get_argument("profile_id", None)
		if not profile_id:
			raise tornado.web.HTTPError(400, "No profile ID was given.")

		if not self.fireinfo.profile_exists(profile_id):
			self._profile_not_found(profile_id)
			return

		self.redirect("/profile/%s" % profile_id)


class FireinfoProfileDetailHandler(FireinfoIndexHandler):
	def get(self, profile_id):
		profile = self.fireinfo.get_profile(profile_id, when=self.when)

		if not profile or not profile.is_showable():
			self._profile_not_found(profile_id)
			return

		self.render("fireinfo/profile-detail.html", profile=profile)


class FireinfoRandomProfileHandler(FireinfoBaseHandler):
	def get(self):
		profile_id = self.fireinfo.get_random_profile(when=self.when)
		if profile_id is None:
			raise tornado.web.HTTPError(404)

		self.redirect("/profile/%s" % profile_id)


class FireinfoDeviceDriverDetail(FireinfoBaseHandler):
	def get(self, driver):
		self.render("fireinfo/driver.html", driver=driver,
			driver_map=self.fireinfo.get_driver_map(driver, when=self.when))


class FireinfoDeviceVendorsHandler(FireinfoBaseHandler):
	def get(self):
		vendors = self.fireinfo.get_vendor_list(when=self.when)

		self.render("fireinfo/vendors.html", vendors=vendors)


class FireinfoStatsHandler(FireinfoBaseHandler):
	def get(self):
		self.render("fireinfo/stats.html")


class FireinfoStatsProcessorsHandler(FireinfoBaseHandler):
	def get(self):
		avg, stddev, min, max = self.fireinfo.get_cpu_clock_speeds(when=self.when)
		arch_map = self.fireinfo.get_arch_map(when=self.when)

		data = {
			"arch_map" : arch_map,
			"cpu_vendors" : self.fireinfo.get_cpu_vendors_map(when=self.when),
			"clock_speed_avg" : avg,
			"clock_speed_stddev" : stddev,
			"clock_speed_min" : min,
			"clock_speed_max" : max,
		}

		return self.render("fireinfo/stats-cpus.html", **data)


class FireinfoStatsProcessorDetailHandler(FireinfoBaseHandler):
	def get(self, platform):
		assert platform in ("arm", "x86")

		flags = self.fireinfo.get_common_cpu_flags_by_platform(platform, when=self.when)

		return self.render("fireinfo/stats-cpus-detail.html",
			platform=platform, flags=flags)


class FireinfoStatsMemoryHandler(FireinfoBaseHandler):
	def get(self):
		avg, stddev, min, max = self.fireinfo.get_memory_amounts(when=self.when)
		amounts = self.fireinfo.get_common_memory_amounts(when=self.when)

		data = {
			"mem_avg" : avg,
			"mem_stddev" : stddev,
			"mem_min" : min,
			"mem_max" : max,
			"amounts" : amounts,
		}

		return self.render("fireinfo/stats-memory.html", **data)


class FireinfoStatsReleasesHandler(FireinfoBaseHandler):
	def get(self):
		data = {
			"releases" : self.fireinfo.get_releases_map(when=self.when),
			"kernels"  : self.fireinfo.get_kernels_map(when=self.when),
		}
		return self.render("fireinfo/stats-oses.html", **data)


class FireinfoStatsVirtualHandler(FireinfoBaseHandler):
	def get(self):
		data = {
			"hypervisors" : self.fireinfo.get_hypervisor_map(when=self.when),
			"virtual"     : self.fireinfo.get_virtual_ratio(when=self.when),
		}

		return self.render("fireinfo/stats-virtual.html", **data)


class FireinfoStatsGeoHandler(FireinfoBaseHandler):
	def get(self):
		return self.render("fireinfo/stats-geo.html",
			geo_locations = self.fireinfo.get_geo_location_map(when=self.when))


class FireinfoStatsLanguagesHandler(FireinfoBaseHandler):
	def get(self):
		return self.render("fireinfo/stats-languages.html",
			languages = self.fireinfo.get_language_map(when=self.when))


class FireinfoStatsNetworkingHandler(FireinfoBaseHandler):
	def get(self):
		network=self.fireinfo.get_network_zones_map(when=self.when)

		return self.render("fireinfo/stats-network.html", network=network)


class FireinfoDeviceVendorHandler(FireinfoBaseHandler):
	def get(self, subsystem, vendor_id):
		devices = self.fireinfo.get_devices_by_vendor(subsystem, vendor_id,
			when=self.when)

		vendor_name = self.fireinfo.get_vendor_string(subsystem, vendor_id)

		self.render("fireinfo/vendor-detail.html", vendor_name=vendor_name,
			devices=devices)


class FireinfoDeviceVendorCompatHandler(FireinfoBaseHandler):
	def get(self, subsystem, vendor_id):
		self.redirect("/device/%s/%s" % (subsystem, vendor_id))


class FireinfoDeviceModelHandler(FireinfoBaseHandler):
	def get(self, subsystem, vendor, model):
		percentage = self.fireinfo.get_device_percentage(subsystem, vendor,
				model, when=self.when)
		percentage *= 100

		profiles = self.fireinfo.get_device_in_profile(subsystem, vendor,
				model, when=self.when)

		vendor_name = self.fireinfo.get_vendor_string(subsystem, vendor)
		model_name = self.fireinfo.get_model_string(subsystem, vendor, model)

		self.render("fireinfo/model-detail.html", profiles=profiles,
			vendor_id=vendor, vendor_name=vendor_name,
			model_id=model, model_name=model_name,
			percentage=percentage, subsystem=subsystem)


class FireinfoDeviceModelCompatHandler(FireinfoBaseHandler):
	def get(self, subsystem, vendor_id, model_id):
		self.redirect("/device/%s/%s/%s" % (subsystem, vendor_id, model_id))


class AdminFireinfoHandler(FireinfoBaseHandler):
	def get(self):
		profiles_with_data, profiles_all = self.fireinfo.get_active_profiles(when=self.when)

		data = {
			"archive_size"       : self.fireinfo.get_archive_size(when=self.when),
			"total_updates"      : self.fireinfo.get_total_updates_count(when=self.when),
			"profiles_with_data" : profiles_with_data,
			"profiles_all"       : profiles_all,
		}

		self.render("fireinfo/stats-admin.html", **data)
