#!/usr/bin/python

from __future__ import division

def format_size(s):
	units = ("B", "k", "M", "G", "T")

	i = 0
	while s >= 1024 and i < len(units) - 1:
		s /= 1024
		i += 1

	return "%.0f%s" % (s, units[i])

def format_time(s):
	#_ = handler.locale.translate
	_ = lambda x: x

	hrs, s = divmod(s, 3600)
	min, s = divmod(s, 60)

	if s >= 30:
		min += 1

	if shorter and not hrs:
		return _("%(min)d min") % { "min" : min }

	return _("%(hrs)d:%(min)02d hrs") % {"hrs" : hrs, "min" : min}
