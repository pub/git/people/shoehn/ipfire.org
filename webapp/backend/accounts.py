#!/usr/bin/python
# encoding: utf-8

import PIL
import StringIO
import hashlib
import ldap
import logging
import urllib

from misc import Object

class Accounts(Object):
	@property
	def ldap(self):
		if not hasattr(self, "_ldap"):
			# Connect to LDAP server
			ldap_uri = self.settings.get("ldap_uri")
			self._ldap = ldap.initialize(ldap_uri)

			# Bind with username and password
			bind_dn = self.settings.get("ldap_bind_dn")
			if bind_dn:
				bind_pw = self.settings.get("ldap_bind_pw", "")
				self._ldap.simple_bind(bind_dn, bind_pw)

		return self._ldap

	def _search(self, query, attrlist=None, limit=0):
		logging.debug("Performing LDAP query: %s" % query)

		search_base = self.settings.get("ldap_search_base")
		results = self.ldap.search_ext_s(search_base, ldap.SCOPE_SUBTREE,
			query, attrlist=attrlist, sizelimit=limit)

		return results

	def search(self, query, limit=0):
		results = self._search(query, limit=limit)

		accounts = []
		for dn, attrs in results:
			account = Account(self.backend, dn, attrs)
			accounts.append(account)

		return sorted(accounts)

	def search_one(self, query):
		result = self.search(query, limit=1)
		assert len(result) <= 1

		if result:
			return result[0]

	def get_all(self):
		# Only return developers (group with ID 500)
		return self.search("(&(objectClass=posixAccount)(gidNumber=500))")

	list = get_all

	def get_by_uid(self, uid):
		return self.search_one("(&(objectClass=posixAccount)(uid=%s))" % uid)

	def get_by_mail(self, mail):
		return self.search_one("(&(objectClass=posixAccount)(mail=%s))" % mail)

	find = get_by_uid

	def find_account(self, s):
		account = self.get_by_uid(s)
		if account:
			return account

		return self.get_by_mail(s)

	def get_by_sip_id(self, sip_id):
		return self.search_one("(|(&(objectClass=sipUser)(sipAuthenticationUser=%s)) \
			(&(objectClass=sipRoutingObject)(sipLocalAddress=%s)))" % (sip_id, sip_id))

	# Session stuff

	def _cleanup_expired_sessions(self):
		self.db.execute("DELETE FROM sessions WHERE time_expires <= NOW()")

	def create_session(self, account, host):
		self._cleanup_expired_sessions()

		res = self.db.get("INSERT INTO sessions(host, uid) VALUES(%s, %s) \
			RETURNING session_id, time_expires", host, account.uid)

		# Session could not be created
		if not res:
			return None, None

		logging.info("Created session %s for %s which expires %s" \
			% (res.session_id, account, res.time_expires))
		return res.session_id, res.time_expires

	def destroy_session(self, session_id, host):
		logging.info("Destroying session %s" % session_id)

		self.db.execute("DELETE FROM sessions \
			WHERE session_id = %s AND host = %s", session_id, host)
		self._cleanup_expired_sessions()

	def get_by_session(self, session_id, host):
		logging.debug("Looking up session %s" % session_id)

		res = self.db.get("SELECT uid FROM sessions WHERE session_id = %s \
			AND host = %s AND NOW() BETWEEN time_created AND time_expires",
			session_id, host)

		# Session does not exist or has expired
		if not res:
			return

		# Update the session expiration time
		self.db.execute("UPDATE sessions SET time_expires = NOW() + INTERVAL '14 days' \
			WHERE session_id = %s AND host = %s", session_id, host)

		return self.get_by_uid(res.uid)
		

class Account(Object):
	def __init__(self, backend, dn, attrs=None):
		Object.__init__(self, backend)
		self.dn = dn

		self.__attrs = attrs or {}

	def __repr__(self):
		return "<%s %s>" % (self.__class__.__name__, self.dn)

	def __cmp__(self, other):
		return cmp(self.name, other.name)

	@property
	def ldap(self):
		return self.accounts.ldap

	@property
	def attributes(self):
		return self.__attrs

	def _get_first_attribute(self, attr, default=None):
		if not self.attributes.has_key(attr):
			return default

		res = self.attributes.get(attr, [])
		if res:
			return res[0]

	def get(self, key):
		try:
			attribute = self.attributes[key]
		except KeyError:
			raise AttributeError(key)

		if len(attribute) == 1:
			return attribute[0]

		return attribute

	def check_password(self, password):
		"""
			Bind to the server with given credentials and return
			true if password is corrent and false if not.

			Raises exceptions from the server on any other errors.
		"""

		logging.debug("Checking credentials for %s" % self.dn)
		try:
			self.ldap.simple_bind_s(self.dn, password.encode("utf-8"))
		except ldap.INVALID_CREDENTIALS:
			logging.debug("Account credentials are invalid.")
			return False

		logging.debug("Successfully authenticated.")
		return True

	def is_admin(self):
		return "admins" in self.groups

	def is_talk_enabled(self):
		return "sipUser" in self.classes or "sipRoutingObject" in self.classes \
			or self.telephone_numbers or self.address

	@property
	def classes(self):
		return self.attributes.get("objectClass", [])

	@property
	def uid(self):
		return self._get_first_attribute("uid")

	@property
	def name(self):
		return self._get_first_attribute("cn")

	@property
	def first_name(self):
		return self._get_first_attribute("givenName")

	@property
	def groups(self):
		if not hasattr(self, "_groups"):
			self._groups = []

			res = self.accounts._search("(&(objectClass=posixGroup) \
				(memberUid=%s))" % self.uid, ["cn"])

			for dn, attrs in res:
				cns = attrs.get("cn")
				if cns:
					self._groups.append(cns[0])

		return self._groups

	@property
	def address(self):
		address = self._get_first_attribute("homePostalAddress", "")
		address = address.replace(", ", "\n")

		return address

	@property
	def email(self):
		name = self.name.lower()
		name = name.replace(" ", ".")
		name = name.replace("Ä", "Ae")
		name = name.replace("Ö", "Oe")
		name = name.replace("Ü", "Ue")
		name = name.replace("ä", "ae")
		name = name.replace("ö", "oe")
		name = name.replace("ü", "ue")

		for mail in self.attributes.get("mail", []):
			if mail.startswith("%s@ipfire.org" % name):
				return mail

		# If everything else fails, we will go with the UID
		return "%s@ipfire.org" % self.uid

	@property
	def sip_id(self):
		if "sipUser" in self.classes:
			return self._get_first_attribute("sipAuthenticationUser")

		if "sipRoutingObject" in self.classes:
			return self._get_first_attribute("sipLocalAddress")

	@property
	def sip_password(self):
		return self._get_first_attribute("sipPassword")

	@property
	def sip_url(self):
		return "%s@ipfire.org" % self.sip_id

	def uses_sip_forwarding(self):
		if self.sip_routing_url:
			return True

		return False

	@property
	def sip_routing_url(self):
		if "sipRoutingObject" in self.classes:
			return self._get_first_attribute("sipRoutingAddress")

	def sip_is_online(self):
		assert self.sip_id

		if not hasattr(self, "_is_online"):
			self._is_online = self.backend.talk.user_is_online(self.sip_id)

		return self._is_online

	@property
	def telephone_numbers(self):
		return self._telephone_numbers + self.mobile_telephone_numbers \
			+ self.home_telephone_numbers

	@property
	def _telephone_numbers(self):
		return self.attributes.get("telephoneNumber") or []

	@property
	def home_telephone_numbers(self):
		return self.attributes.get("homePhone") or []

	@property
	def mobile_telephone_numbers(self):
		return self.attributes.get("mobile") or []

	def avatar_url(self, size=None):
		if self.backend.debug:
			hostname = "accounts.dev.ipfire.org"
		else:
			hostname = "accounts.ipfire.org"

		url = "http://%s/avatar/%s.jpg" % (hostname, self.uid)

		if size:
			url += "?size=%s" % size

		return url

	gravatar_icon = avatar_url

	def get_avatar(self, size=None):
		avatar = self._get_first_attribute("jpegPhoto")
		if not avatar:
			return

		if not size:
			return avatar

		return self._resize_avatar(avatar, size)

	def _resize_avatar(self, image, size):
		image = StringIO.StringIO(image)
		image = PIL.Image.open(image)

		# Resize the image to the desired resolution
		image.thumbnail((size, size), PIL.Image.ANTIALIAS)

		f = StringIO.StringIO()

		# If writing out the image does not work with optimization,
		# we try to write it out without any optimization.
		try:
			image.save(f, "JPEG", optimize=True)
		except:
			image.save(f, "JPEG")

		return f.getvalue()

	def get_gravatar_url(self, size=128):
		try:
			gravatar_email = self.email.lower()
		except:
			gravatar_email = "nobody@ipfire.org"

		# construct the url
		gravatar_url = "http://www.gravatar.com/avatar/" + \
			hashlib.md5(gravatar_email).hexdigest() + "?"
		gravatar_url += urllib.urlencode({'d': "mm", 's': str(size)})

		return gravatar_url


if __name__ == "__main__":
	a = Accounts()

	print a.list()
