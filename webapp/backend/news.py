#!/usr/bin/python

from misc import Object

class News(Object):
	def get(self, uuid, locale=None):
		res = self.db.get("SELECT * FROM news WHERE uuid = %s \
			AND published IS NOT NULL AND published <= NOW()", uuid)

		if res and locale:
			res = self._translate_news_one(res, locale=locale)

		return res

	def get_by_id(self, id, locale=None):
		res = self.db.get("SELECT * FROM news WHERE id = %s \
			AND published IS NOT NULL AND published <= NOW()", id)

		if res and locale:
			res = self._translate_news_one(res, locale=locale)

		return res

	def get_by_slug(self, slug, locale=None):
		res = self.db.get("SELECT * FROM news WHERE slug = %s \
			AND published IS NOT NULL AND published <= NOW()", slug)

		if res:
			return res

		res = self.db.get("SELECT news_id FROM news_translations WHERE slug = %s", slug)
		if res:
			return self.get_by_id(res.news_id, locale=locale)

	def get_latest(self, author=None, locale=None, limit=1, offset=0):
		query = "SELECT * FROM news WHERE published IS NOT NULL AND published <= NOW()"
		args = []

		if author:
			query += " AND author_id = %s"
			args.append(author)

		query += " ORDER BY published DESC"

		if limit:
			query += " LIMIT %s"
			args.append(limit)

			if offset:
				query += " OFFSET %s"
				args.append(offset)

		news = self.db.query(query, *args)

		if locale:
			news = self._translate_news(news, locale=locale)

		return news

	def get_by_year(self, year, locale=None):
		res = self.db.query("SELECT * FROM news \
			WHERE published IS NOT NULL AND published <= NOW() \
			AND EXTRACT(YEAR FROM published) = %s ORDER BY published DESC", year)

		if res and locale:
			res = self._translate_news(res, locale=locale)

		return res

	def _translate_news(self, news, locale):
		ret = []

		for n in news:
			n = self._translate_news_one(n, locale)
			ret.append(n)

		return ret

	def _translate_news_one(self, news, locale):
		lang = locale.code[:2]

		res = self.db.get("SELECT * FROM news_translations \
			WHERE news_id = %s AND lang = %s", news.id, lang)

		if res:
			news.update(res)

		return news

	@property
	def years(self):
		query = self.db.query("SELECT DISTINCT EXTRACT(YEAR FROM published)::integer AS year FROM news \
			WHERE published IS NOT NULL AND published <= NOW() ORDER BY year DESC")

		return [r.year for r in query]
