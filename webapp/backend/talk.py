#!/usr/bin/python

import re

import database

from misc import Object

class Talk(Object):
	def init(self):
		db_args = self.backend.db._db_args
		db_args.update({
			"database" : "kamailio",
		})

		self._db = database.Connection(**db_args)

	@property
	def db(self):
		return self._db

	def get_phonebook(self, account=None):
		accounts = []
		for a in self.accounts.list():
			if account and a == account:
				continue

			if not a.is_talk_enabled():
				continue

			accounts.append(a)

		return accounts

	def user_is_online(self, sip_id):
		res = self.db.get("SELECT 1 FROM location WHERE username = %s \
			AND expires >= NOW() LIMIT 1", sip_id)

		if res:
			return True

		return False

	def get_lines(self, account=None):
		accounts_cache = {}

		if account and not account.is_talk_enabled():
			return []

		if account:
			res = self.db.query("SELECT contact AS location, expires, user_agent, socket \
				FROM location WHERE domain = %s AND username = %s ORDER BY expires DESC",
				"ipfire.org", account.sip_id)
		else:
			res = self.db.query("SELECT username, domain, contact AS location, \
				expires, user_agent, socket FROM location ORDER BY username, expires DESC")

		result = []
		for row in res:
			if account:
				row.account = account
			elif row.username:
				try:
					row.account = accounts_cache[row.username]
				except KeyError:
					row.account = accounts_cache[row.username] = \
						self.accounts.get_by_sip_id(row.username)
			else:
				row.account = None

			# Check if TLS is used
			row.tls_enabled = row.socket.startswith("tls:")

			# Cut off everything after ;
			row.location, sep, rest = row.location.partition(";")

			result.append(row)

		return result

	def _process_sip_uri(self, sip_uri):
		sip_uri, delimiter, rest = sip_uri.partition(";")

		m = re.match(r"^sip:([0-9]{4})@ipfire\.org", sip_uri)
		if m:
			return m.group(1)

		# Remove trailing default port
		if sip_uri.endswith(":5060"):
			sip_uri = sip_uri.replace(":5060", "")

		return sip_uri

	def _process_cdr(self, entries, replace_sip_uris=False):
		accounts_cache = {}

		result = []
		for row in entries:
			if replace_sip_uris:
				row["caller"] = self._process_sip_uri(row.caller)

			try:
				row["caller_account"] = accounts_cache[row.caller]
			except KeyError:
				row["caller_account"] = accounts_cache[row.caller] = \
					self.accounts.get_by_sip_id(row.caller)

			if replace_sip_uris:
				row["called"] = self._process_sip_uri(row.called)

			try:
				row["called_account"] = accounts_cache[row.called]
			except KeyError:
				row["called_account"] = accounts_cache[row.called] = \
					self.accounts.get_by_sip_id(row.called)

			if not row.called_account:
				row["called_conference"] = self.get_conference(row.called)
			else:
				row["called_conference"] = None

			try:
				row["time"] = datetime.datetime.fromutctimestamp(row.time)
			except:
				pass

			result.append(row)

		return result

	def get_call_log(self, account, limit=25):
		if account:
			res = self.db.query("SELECT * FROM cdr WHERE (caller = %s OR called = %s) \
				ORDER BY time DESC LIMIT %s", account.sip_id, account.sip_id, limit)
		else:
			res = self.db.query("SELECT * FROM cdr ORDER BY time DESC LIMIT %s", limit)

		return self._process_cdr(res)

	def get_ongoing_calls(self, account=None, sip_id=None):
		if account and sip_id is None:
			sip_id = account.sip_id

			# If the given account does not have a SIP ID,
			# we not need to search for any active sessions
			if sip_id is None:
				return []

		if sip_id:
			sip_id = "sip:%s@%%" % sip_id

			res = self.db.query("SELECT from_uri AS caller, \
				to_uri AS called, start_time AS time FROM dialog \
				WHERE from_uri LIKE %s OR to_uri LIKE %s ORDER BY time",
				sip_id, sip_id)
		else:
			res = self.db.query("SELECT from_uri AS caller, to_uri AS called, \
				start_time AS time FROM dialog ORDER BY time")

		return self._process_cdr(res, replace_sip_uris=True)

	def initiate_call(self, caller, called, when=None):
		self.db.execute("INSERT INTO dialout(caller, called, not_before) \
			VALUES(%s, %s, %s)", caller, called, when)

	# Favourites

	def get_favourite_contacts(self, account, limit=6):
		res = self.db.query("SELECT src_user AS caller, dst_ouser AS called, \
			COUNT(*) AS count FROM acc WHERE method = %s AND src_user = %s AND \
			dst_ouser BETWEEN %s AND %s AND time >= NOW() - INTERVAL '1 year' \
			GROUP BY caller, called ORDER BY count DESC LIMIT %s",
			"INVITE", account.sip_id, "1000", "1999", limit)

		return self._process_cdr(res)

	# Conferences

	@property
	def conferences(self):
		conferences = []

		for no in range(1, 10):
			conference = Conference(self.backend, no)
			conferences.append(conference)

		return conferences

	def get_conference(self, id):
		for c in self.conferences:
			if not c.sip_id == id:
				continue

			return c


class Conference(Object):
	def __init__(self, backend, no):
		Object.__init__(self, backend)
		self.no = no

	def __cmp__(self, other):
		return cmp(self.no, other.no)

	@property
	def name(self):
		return "IPFire Conference Room %s" % self.no

	@property
	def sip_id(self):
		return "%s" % (9000 + self.no)

	@property
	def sip_url(self):
		return "%s@ipfire.org" % self.sip_id

	@property
	def participants(self):
		if not hasattr(self, "_participants"):
			self._participants = self.backend.talk.get_ongoing_calls(sip_id=self.sip_id)

		return self._participants

	@property
	def invitees(self):
		invitees = []
		participants = [p.caller for p in self.participants]

		for invitee in self.backend.talk.get_phonebook():
			if not invitee.sip_id:
				continue

			if invitee.sip_id in participants:
				continue

			invitees.append(invitee)

		return invitees
