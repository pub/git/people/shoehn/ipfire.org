#!/usr/bin/python

import magic

from misc import Object

class Nopaste(Object):
	def create(self, subject, content, type="text", expires=None, account=None, address=None):
		self._cleanup_database()

		uid = None
		if account:
			uid = account.uid

		# Escape any backslashes. PostgreSQL tends to think that they lead octal
		# values or something that confuses the convertion from text to bytea.
		if type == "text":
			content = content.replace("\\", "\\\\")
			mimetype = "text/plain"

		elif type == "file":
			content = buffer(content)
			mimetype = self._guess_mimetype(content)

		# http://blog.00null.net/easily-generating-random-strings-in-postgresql/
		res = self.db.get("INSERT INTO nopaste(uuid, subject, content, time_expires, address, \
			uid, mimetype, size) VALUES(random_slug(), %s, %s, \
				(CASE WHEN %s = 0 THEN NULL ELSE NOW() + INTERVAL '%s seconds' END), \
			%s, %s, %s, %s) RETURNING uuid",
			subject, content, expires, expires, address, uid, mimetype, len(content))

		if res:
			return res.uuid

	def _guess_mimetype(self, buf):
		ms = magic.open(magic.NONE)
		ms.load()

		# Return the mime type
		ms.setflags(magic.MAGIC_MIME_TYPE)

		buf = "%s" % buf

		try:
			return ms.buffer(buf)
		finally:
			ms.close()

	def get(self, uuid):
		res = self.db.get("SELECT uuid, subject, time_created, time_expires, address, uid, \
			mimetype, views, size FROM nopaste WHERE uuid = %s AND (CASE WHEN time_expires \
			IS NULL THEN TRUE ELSE NOW() < time_expires END)", uuid)

		if res:
			# Get the account that uploaded this if available
			res.account = None
			if res.uid:
				res.account = self.backend.accounts.get_by_uid(res.uid)

			# Touch the entry so it won't be deleted when it is still used
			self._update_lastseen(uuid)

		return res

	def get_content(self, uuid):
		# Try fetching the object from memcache. If found, we return it right away.
		content = self.memcache.get("nopaste-%s" % uuid)

		# If the object was not in the cache, we need to fetch it from the database.
		if not content:
			res = self.db.get("SELECT content, views FROM nopaste WHERE uuid = %s", uuid)

			# Convert the content to a byte string
			content = "%s" % res.content

			# Save it in the cache for later when it has been requested a couple
			# of times
			if res.views >= 5:
				self.memcache.set("nopaste-%s" % uuid, content, 6 * 3600)

		return content

	def _update_lastseen(self, uuid):
		self.db.execute("UPDATE nopaste SET time_lastseen = NOW(), views = views + 1 \
			WHERE uuid = %s", uuid)

	def _cleanup_database(self):
		# Delete old pastes when they are expired or when they have not been
		# accessed in a long time.
		self.db.execute("DELETE FROM nopaste WHERE (CASE \
			WHEN time_expires IS NULL \
				THEN time_lastseen + INTERVAL '6 months' <= NOW() \
			ELSE NOW() >= time_expires END)")
