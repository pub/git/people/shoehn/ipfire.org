#!/usr/bin/python

import ConfigParser as configparser

import accounts
import ads
import database
import geoip
import fireinfo
import iuse
import memcached
import mirrors
import netboot
import nopaste
import news
import planet
import releases
import settings
import talk
import tracker
import wishlist

class Backend(object):
	def __init__(self, configfile, debug=False):
		# Read configuration file.
		self.config = self.read_config(configfile)
		self.debug = debug

		# Setup database.
		self.setup_database()

		# Initialize settings first.
		self.settings = settings.Settings(self)
		self.memcache = memcached.Memcached(self)

		# Initialize backend modules.
		self.accounts = accounts.Accounts(self)
		self.advertisements = ads.Advertisements(self)
		self.downloads = mirrors.Downloads(self)
		self.geoip = geoip.GeoIP(self)
		self.fireinfo = fireinfo.Fireinfo(self)
		self.iuse = iuse.IUse(self)
		self.mirrors = mirrors.Mirrors(self)
		self.netboot = netboot.NetBoot(self)
		self.nopaste = nopaste.Nopaste(self)
		self.news = news.News(self)
		self.planet = planet.Planet(self)
		self.releases = releases.Releases(self)
		self.talk = talk.Talk(self)
		self.tracker = tracker.Tracker(self)
		self.wishlist = wishlist.Wishlist(self)

	def read_config(self, configfile):
		cp = configparser.ConfigParser()
		cp.read(configfile)

		return cp

	def setup_database(self):
		"""
			Sets up the database connection.
		"""
		credentials = {
			"host"     : self.config.get("database", "server"),
			"database" : self.config.get("database", "database"),
			"user"     : self.config.get("database", "username"),
			"password" : self.config.get("database", "password"),
		}

		self.db = database.Connection(**credentials)
