#!/usr/bin/python

#import httplib
#import logging
#import markdown2
import operator
import os
#import random
#import re
#import socket
#import time
#import tornado.database
#import tornado.locale
import tornado.web
#import unicodedata

import backend

from handlers_accounts import *
from handlers_admin import *
from handlers_base import *
from handlers_boot import *
from handlers_download import *
from handlers_fireinfo import *
from handlers_iuse import *
from handlers_mirrors import *
from handlers_news import *
from handlers_nopaste import *
from handlers_planet import *
from handlers_rss import *
from handlers_talk import *
from handlers_tracker import *
from handlers_wishlist import *

class RootHandler(BaseHandler):
	"""
		This handler redirects any request directly to /.

		It can be used to be compatible with some ancient index urls.
	"""
	def get(self, *args):
		self.redirect("/")


class LangCompatHandler(BaseHandler):
	"""
		Redirect links in the old format to current site:

		E.g. /en/index -> /index
	"""
	def get(self, lang, page):
		self.redirect("/%s" % page)


class IndexHandler(BaseHandler):
	rss_url = "/news.rss"

	"""
		This handler displays the welcome page.
	"""
	def get(self):
		# Get the latest release.
		latest_release = self.releases.get_latest()

		# Get a list of the most recent news items and put them on the page.		
		latest_news = self.get_latest_news()

		# Interesting items from the wishlist.
		hottest_wish = self.wishlist.get_hottest_wish()

		return self.render("index.html", latest_news=latest_news,
			latest_release=latest_release, hottest_wish=hottest_wish)

	def get_latest_news(self, count=5):
		latest_news = []

		for news in self.news.get_latest(limit=count):
			latest_news.append(("news", news))

		for post in self.planet.get_entries(limit=count):
			latest_news.append(("planet", post))

		latest_news.sort(key=operator.itemgetter(1), reverse=True)

		return latest_news[:count]


class StaticHandler(BaseHandler):
	"""
		This handler shows the files that are in plain html format.
	"""
	@property
	def static_path(self):
		return os.path.join(self.application.settings["template_path"], "static")

	@property
	def static_files(self):
		for dir, subdirs, files in os.walk(self.static_path):
			dir = dir[len(self.static_path) + 1:]
			for file in files:
				if not file.endswith(".html"):
					continue
				yield os.path.join(dir, file)

	def get(self, name=None):
		name = "%s.html" % name

		if not name in self.static_files:
			raise tornado.web.HTTPError(404)

		self.render("static/%s" % name, lang=self.locale.code[:2])


class GeoIPHandler(BaseHandler):
	def get_address(self):
		addr = self.get_argument("addr", None)

		if not addr:
			addr = self.get_remote_ip()

		return addr

	def get(self):
		addr = self.get_address()

		peer = self.geoip.get_all(addr)
		if peer:
			peer["country_name"] = self.geoip.get_country_name(peer.country)

		mirrors = self.mirrors.get_for_location(peer)

		self.render("geoip/index.html", addr=addr, peer=peer, mirrors=mirrors)


class DonateHandler(BaseHandler):
	def get(self):
		reason_for_transfer = self.get_argument("reason_for_transfer", None)

		# Interesting items from the wishlist.
		wishlist_items = self.wishlist.get_hot_wishes()

		self.render("donate.html", wishlist_items=wishlist_items,
			reason_for_transfer=reason_for_transfer)


class DownloadHandler(BaseHandler):
	def get(self):
		release = self.releases.get_latest()

		self.render("download.html", release=release)
