#!/usr/bin/python

import tornado.web

from handlers_base import *

class PlanetBaseHandler(BaseHandler):
	def group_entries_by_month(self, entries):
		months = {}

		for entry in entries:
			key = (entry.published.year, entry.published.month)

			try:
				months[key].append(entry)
			except KeyError:
				months[key] = [entry]

		months = months.items()
		months.sort(reverse=True)

		return months


class PlanetMainHandler(PlanetBaseHandler):
	rss_url = "/rss"

	def get(self):
		offset = int(self.get_argument("offset", 0))
		limit = int(self.get_argument("limit", 4))

		entries = self.planet.get_entries(offset=offset, limit=limit)

		self.render("planet/index.html", entries=entries, offset=offset + limit, limit=limit)


class PlanetHotEntriesHandler(PlanetBaseHandler):
	def get(self):
		days = self.get_argument("days", None)
		try:
			days = int(days)
		except (TypeError, ValueError):
			days = 30

		entries = self.planet.get_hot_entries(days, limit=25)

		self.render("planet/hottest.html", entries=entries)


class PlanetUserHandler(PlanetBaseHandler):
	def get(self, author):
		author = self.accounts.get_by_uid(author)
		if not author:
			raise tornado.web.HTTPError(404, "User is unknown")

		entries = self.planet.get_entries_by_author(author.uid)
		entries = self.group_entries_by_month(entries)

		self.render("planet/list.html", author=author, year=None,
			entries=entries, rss_url="/user/%s/rss" % author.uid)


class PlanetPostingHandler(PlanetBaseHandler):
	def get(self, slug):
		self.entry = self.planet.get_entry_by_slug(slug)

		if not self.entry:
			raise tornado.web.HTTPError(404)

		self.render("planet/posting.html",
			author=self.entry.author, entry=self.entry)

	def on_finish(self):
		assert self.entry

		# Get the referer and location for statistical purposes
		referer = self.request.headers.get("Referer", None)
		location = self.get_remote_location()
		if location:
			location = location.country

		self.entry.count_view(referer=referer, location=location)


class PlanetSearchHandler(PlanetBaseHandler):
	def get(self):
		query = self.get_argument("q", "")

		if query:
			entries = self.planet.search(query)
		else:
			entries = []

		self.render("planet/search.html", entries=entries, query=query)


class PlanetYearHandler(PlanetBaseHandler):
	def get(self, year):
		entries = self.planet.get_entries_by_year(year)
		entries = self.group_entries_by_month(entries)

		self.render("planet/list.html", author=None, year=year,
			entries=entries)
