#!/usr/bin/python

import logging
import os
import tornado.httpserver
import tornado.ioloop
import tornado.locale
import tornado.options
import tornado.web

import backend

from handlers_base import BaseHandler

BASEDIR = os.path.dirname(__file__)

def word_wrap(s, width=45):
	paragraphs = s.split('\n')
	lines = []
	for paragraph in paragraphs:
		while len(paragraph) > width:
			pos = paragraph.rfind(' ', 0, width)
			if not pos:
				pos = width
			lines.append(paragraph[:pos])
			paragraph = paragraph[pos:]
		lines.append(paragraph.lstrip())
	return '\n'.join(lines)

class BootBaseHandler(BaseHandler):
	@property
	def arch(self):
		arch = self.get_argument("arch", "i386")
		if not arch in ("x86_64", "i586", "i386"):
			raise tornado.web.HTTPError(400, "Invalid architecture")

		if arch == "i386":
			arch = "i586"

		return arch

	@property
	def platform(self):
		platform = self.get_argument("platform", "pcbios")
		if not platform in ("pcbios", "efi"):
			raise tornado.web.HTTPError(400, "Invalid platform")

		return platform


class MenuGPXEHandler(BootBaseHandler):
	"""
		menu.gpxe
	"""
	def get(self):
		# Check if version of the bootloader is recent enough.
		# Otherwise send the latest version of the PXE loader.
		user_agent = self.request.headers.get("User-Agent", None)
		if user_agent:
			try:
				client, version = user_agent.split("/")
			except:
				pass
			else:
				# We replaced gPXE by iPXE.
				if client == "gPXE":
					return self.serve_update()

				# Everything under version 1.0.0 should be
				# updated.
				if version < "1.0.0":
					return self.serve_update()

				# This is an outdated git build
				if "c4bce" in version:
					return self.serve_update()

		# Deliver content
		self.set_header("Content-Type", "text/plain")
		self.write("#!gpxe\n")

		self.write("set 209:string premenu.cfg?arch=%s&platform=%s\n" \
			% (self.arch, self.platform))
		self.write("set 210:string http://boot.ipfire.org/\n")
		self.write("chain pxelinux.0\n")

	def serve_update(self):
		self.set_header("Content-Type", "text/plain")
		self.write("#!gpxe\n")

		# Small warning
		self.write("echo\necho Your copy of gPXE/iPXE is too old. ")
		self.write("Upgrade to avoid seeing this every boot!\n")

		self.write("chain http://mirror0.ipfire.org/releases/ipfire-boot/latest/ipxe.kpxe\n")


class PremenuCfgHandler(BootBaseHandler):
	def get(self):
		self.set_header("Content-Type", "text/plain")

		self.render("netboot/premenu.cfg", arch=self.arch, platform=self.platform)


class MenuCfgHandler(BootBaseHandler):
	class NightlyBuildReleaseDummy(object):
		id = 100
		name = "Nightly Build"
		sname = "nightly-build"

		def supports_arch(self, arch):
			return arch in ("i586", "x86_64")

		def supports_platform(self, platform):
			return platform == "pcbios"

		def netboot_kernel_url(self, arch, platform):
			return "http://nightly.ipfire.org/next/latest/%s/images/vmlinuz" % arch

		def netboot_initrd_url(self, arch, platform):
			return "http://nightly.ipfire.org/next/latest/%s/images/instroot" % arch

		def netboot_args(self, arch, platform):
			return "installer.download-url=http://nightly.ipfire.org/next/latest/%s/images/installer.iso" % arch

		def is_netboot_capable(self):
			return True

	nightly_build = NightlyBuildReleaseDummy()

	def get(self):
		self.set_header("Content-Type", "text/plain")

		latest_release = self.releases.get_latest()
		if latest_release and not latest_release.supports_arch(self.arch):
			latest_release = None

		stable_releases = [r for r in self.releases.get_stable()
			if r.supports_arch(self.arch) and r.supports_platform(self.platform)]
		try:
			stable_releases.remove(latest_release)
		except ValueError:
			pass

		development_releases = [r for r in self.releases.get_unstable()
			if r.supports_arch(self.arch) and r.supports_platform(self.platform)]
		development_releases.insert(0, self.nightly_build)

		self.render("netboot/menu.cfg", latest_release=latest_release,
			stable_releases=stable_releases, development_releases=development_releases,
			arch=self.arch, platform=self.platform)
