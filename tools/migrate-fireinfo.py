#!/usr/bin/python

import pymongo

import backend

DATABASE_HOSTS = [
	"falco.ipfire.org",
	"wilhelmina.ipfire.org",
	"miranda.ipfire.org",
]

connection = pymongo.Connection(DATABASE_HOSTS)
db = connection["stasy"]

b = backend.Backend("webapp.conf")

c = db.archives.find().sort("updated", pymongo.ASCENDING)

for profile in c:
	public_id = profile.get("public_id")
	when = profile.get("updated")

	print public_id

	location = None
	if profile.has_key("geoip") and profile["geoip"]:
		location = profile["geoip"]["country_code"]

	print "Importing profile %s..." % public_id
	try:
		b.fireinfo.handle_profile(public_id, profile, location=location, when=when)
	except:
		print profile
		raise
